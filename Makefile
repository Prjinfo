# Makefile pour le développement du projet d'informatique II 2010
# Damien Nguyen et Etienne Wodey

SHELL = /bin/sh
.PHONY: clean, mrproper
.SUFFIXES:

# testing module
.PHONY: testing-math-vector
testing-math-vector:
	make -C testing math-vector

.PHONY: testing-math-vector3
testing-math-vector3:
	make -C testing math-vector3

.PHONY: testing-math-sqmatrix3
testing-math-sqmatrix3:
	make -C testing math-sqmatrix3

# math lib
.PHONY: math
math:
	make -C math

# cleaning directories
clean:
	make -C math clean
	make -C testing clean

mrproper: clean
	make -C math mrproper

